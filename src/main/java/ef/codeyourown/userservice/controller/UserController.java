package ef.codeyourown.userservice.controller;

import ef.codeyourown.userservice.models.User;
import ef.codeyourown.userservice.services.UserService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/api/user")
public class UserController {
    private final UserService userservice;
    private final PasswordEncoder passwordEncoder;

    public UserController(UserService userservice, PasswordEncoder passwordEncoder) {
        this.userservice = userservice;
        this.passwordEncoder = passwordEncoder;
    }

    //Endpoint Mapping underneath
    @GetMapping()
    public User getUserByMail(@RequestParam("mail") String mail) {
        return this.userservice.findUserByMail(mail);
    }

    //myservice.com/api/user -> POST
    @PostMapping()
    public void addUser(@RequestBody User user) {
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        userservice.addUser(user);
    }
}
