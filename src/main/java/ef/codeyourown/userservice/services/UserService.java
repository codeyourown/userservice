package ef.codeyourown.userservice.services;

import ef.codeyourown.userservice.models.User;

import java.util.List;

public interface UserService {

//    List<User> getUsers(); -> To be implemented

    User addUser(User user);

    User findUserByMail(String mail);
}
