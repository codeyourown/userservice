package ef.codeyourown.userservice.services;

import ef.codeyourown.userservice.models.User;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {
    //Store users at RunTime
    private List<User> users = new ArrayList<>();

    @Override
    public User addUser(User user) {
        this.users.add(user);
        return user;
    }

    @Override
    public User findUserByMail(String mail) {
        return null;
    }
}
